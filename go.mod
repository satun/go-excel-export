module gitlab.com/satun/go-excel-export

go 1.12

require (
	cloud.google.com/go v0.39.0
	github.com/360EntSecGroup-Skylar/excelize/v2 v2.0.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-sql-driver/mysql v1.4.1
	github.com/joho/godotenv v1.3.0
	github.com/labstack/echo/v4 v4.1.5
	golang.org/x/crypto v0.0.0-20190510104115-cbcb75029529 // indirect
	google.golang.org/appengine v1.5.0
)
