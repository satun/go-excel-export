package handler

import (
	"github.com/360EntSecGroup-Skylar/excelize/v2"
	"github.com/labstack/echo/v4"
	"log"
	"net/http"
	"strings"
	"time"

	"context"

	"gitlab.com/satun/go-excel-export/model"

	"cloud.google.com/go/storage"
)

func (h *Handler) AutoExport(c echo.Context) (err error) {
	request := new(model.Request)

	if err := c.Bind(request); err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	db, err := h.Config.SetDBConnect()
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	rows, err := db.Query(request.Sql)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}
	defer db.Close()

	columns, _ := rows.Columns()

	/////
	f := excelize.NewFile()

	fileName := strings.Join([]string{"export", time.Now().Format("_2006-01-02_15:04:05"), ".xlsx"}, "")

	sheet := "Sheet1"
	irow := 1
	//icol := 1
	cor := ""

	if err := drawFilters(f, sheet, &irow, request.Filters); err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	if err := drawHeader(f, sheet, &irow, request.Fields); err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}
	/////

	for rows.Next() {
		row := make([]interface{}, len(columns))
		for idx := range columns {
			row[idx] = new(model.Scanner)
		}

		err := rows.Scan(row...)
		if err != nil {
			c.Logger().Error(err)
		}

		for icol, field := range request.Fields {
			for idx, column := range columns {
				if field.Name == column {
					var scanner = row[idx].(*model.Scanner)

					cor, _ = excelize.CoordinatesToCellName(icol+1, irow)

					err = f.SetCellValue(sheet, cor, scanner.Value)
					if err = rows.Err(); err != nil {
						c.Logger().Error(err)
						return c.JSON(http.StatusBadRequest, err)
					}
				}
			}
		}

		irow++
	}

	if err = rows.Err(); err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	if err = setStyles(f, request, sheet, irow); err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	/////GCP
	ctx := context.Background()

	// Creates a client.
	client, err := storage.NewClient(ctx)
	if err != nil {
		c.Logger().Error(err)
		return c.JSON(http.StatusBadRequest, err)
	}

	// Sets the name for the new bucket.
	bucketName := "export-excel-files"

	// Creates a Bucket instance.
	bucket := client.Bucket(bucketName)

	w := bucket.Object(fileName).NewWriter(ctx)
	w.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	w.ContentType = "Content-Type: application/xlsx; charset=utf-8"
	w.ContentDisposition = strings.Join([]string{"Content-Disposition: attachment; filename=", fileName}, "")

	err = f.Write(w)
	if err != nil {
		log.Fatal(err)
	}

	if err := w.Close(); err != nil {
		log.Fatal(err)
	}
	/////

	const publicURL = "https://storage.googleapis.com/"
	link := strings.Join([]string{publicURL, bucketName, "/", fileName}, "")

	return c.String(http.StatusOK, link)
}

func drawFilters(f *excelize.File, sheet string, row *int, filters []model.Filter) error {
	for _, filter := range filters {
		cor, _ := excelize.CoordinatesToCellName(1, *row)

		err := f.SetCellValue(sheet, cor, filter.Description)
		if err != nil {
			return err
		}

		cor, _ = excelize.CoordinatesToCellName(2, *row)

		err = f.SetCellValue(sheet, cor, filter.Value)
		if err != nil {
			return err
		}

		*row++
	}

	*row++

	return nil
}

func drawHeader(f *excelize.File, sheet string, row *int, fields []model.Field) error {
	for icol, field := range fields {
		cor, _ := excelize.CoordinatesToCellName(icol+1, *row)

		err := f.SetCellValue(sheet, cor, field.Description)
		if err != nil {
			return err
		}
	}

	*row++

	return nil
}

func setStyles(f *excelize.File, r *model.Request, sheet string, irow int) error {
	//BODY
	style, err := f.NewStyle(`{"font":{"family":"Calibri","size":11}}`)
	if err != nil {
		return err
	}

	corStart, _ := excelize.CoordinatesToCellName(1, 1)
	corEnd, _ := excelize.CoordinatesToCellName(len(r.Fields), irow)

	err = f.SetCellStyle(sheet, corStart, corEnd, style)
	if err != nil {
		return err
	}
	//

	//HEAD
	style, err = f.NewStyle(`{"border":[
	{"type":"left","color":"000000","style":1},
	{"type":"top","color":"000000","style":1},
	{"type":"bottom","color":"000000","style":1},
	{"type":"right","color":"000000","style":1}
	],"font":{"bold":true,"family":"Calibri","size":11}}`)
	if err != nil {
		return err
	}

	corStart, _ = excelize.CoordinatesToCellName(1, len(r.Filters)+2)
	corEnd, _ = excelize.CoordinatesToCellName(len(r.Fields), len(r.Filters)+2)

	err = f.SetCellStyle(sheet, corStart, corEnd, style)
	if err != nil {
		return err
	}
	//

	return nil
}

//func deleteFile(path string) {
//	err := os.Remove(path)
//	if err != nil {
//		log.Fatal(err)
//	}
//}

//WORK WITH LOCAL DIR
//func (h *Handler) AutoExport(c echo.Context) (err error) {
//	request := new(model.Request)
//
//	if err := c.Bind(request); err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//
//	rows, err := h.DB.Query(request.Sql)
//	if err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//	defer h.DB.Close()
//
//	columns, _ := rows.Columns()
//
//	/////
//	f := excelize.NewFile()
//
//	fileName := strings.Join([]string{h.Config.TmpDir, "export", time.Now().Format("_2006-01-02_15:04:05"), ".xlsx"}, "")
//
//	sheet := "Sheet1"
//	irow := 1
//	//icol := 1
//	cor := ""
//
//	if err := drawFilters(f, sheet, &irow, request.Filters); err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//
//	if err := drawHeader(f, sheet, &irow, request.Fields); err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//	/////
//
//	for rows.Next() {
//		row := make([]interface{}, len(columns))
//		for idx := range columns {
//			row[idx] = new(model.Scanner)
//		}
//
//		err := rows.Scan(row...)
//		if err != nil {
//			c.Logger().Error(err)
//		}
//
//		for icol, field := range request.Fields {
//			for idx, column := range columns {
//				if field.Name == column {
//					var scanner = row[idx].(*model.Scanner)
//
//					cor, _ = excelize.CoordinatesToCellName(icol+1, irow)
//
//					err = f.SetCellValue(sheet, cor, scanner.Value)
//					if err = rows.Err(); err != nil {
//						c.Logger().Error(err)
//						return c.JSON(http.StatusBadRequest, err)
//					}
//				}
//			}
//		}
//
//		irow++
//	}
//
//	if err = rows.Err(); err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//
//	if err = setStyles(f, request, sheet, irow); err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//
//	err = f.SaveAs(fileName)
//	if err != nil {
//		c.Logger().Error(err)
//		return c.JSON(http.StatusBadRequest, err)
//	}
//	defer deleteFile(fileName)
//
//	return c.File(fileName)
//}
