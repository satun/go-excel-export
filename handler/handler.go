package handler

import (
	"database/sql"

	"gitlab.com/satun/go-excel-export/config"
)

type Handler struct {
	DB     *sql.DB
	Config *config.Config
}
