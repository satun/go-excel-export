package config

import (
	"database/sql"
	"log"
	"os"
	"strings"
)

type Config struct {
	Server Server
	Mysql  Mysql
	GCP    GCP
	LogDir string
	TmpDir string
}

type Server struct {
	Host string
	Port string
}

type Mysql struct {
	Host     string
	Port     string
	User     string
	Password string
	Database string
}

type GCP struct {
	ProjectID string
}

func New() *Config {
	serverHost := os.Getenv("SERVER_HOST")
	projectID := os.Getenv("PROJECT_ID")
	//if serverHost == "" {
	//	log.Fatal("Config variable SERVER_HOST not defined!")
	//}

	serverPort := os.Getenv("SERVER_PORT")
	if serverPort == "" {
		log.Fatal("Config variable SERVER_PORT not defined!")
	}

	mysqlHost := os.Getenv("MYSQL_HOST")
	if mysqlHost == "" {
		log.Fatal("Config variable MYSQL_HOST not defined!")
	}

	mysqlPort := os.Getenv("MYSQL_PORT")
	if mysqlPort == "" {
		log.Fatal("Config variable MYSQL_PORT not defined!")
	}

	mysqlUser := os.Getenv("MYSQL_USER")
	if mysqlUser == "" {
		log.Fatal("Config variable MYSQL_USER not defined!")
	}

	mysqlPassword := os.Getenv("MYSQL_PASSWORD")
	if mysqlPassword == "" {
		log.Fatal("Config variable MYSQL_PASSWORD not defined!")
	}

	mysqlDatabase := os.Getenv("MYSQL_DATABASE")
	if mysqlDatabase == "" {
		log.Fatal("Config variable MYSQL_DATABASE not defined!")
	}

	logDir := os.Getenv("LOG_DIR")
	if logDir == "" {
		log.Fatal("Config variable LOG_DIR not defined!")
	}

	tmpDir := os.Getenv("TMP_DIR")
	if logDir == "" {
		log.Fatal("Config variable TMP_DIR not defined!")
	}

	return &Config{
		Server: Server{
			Host: serverHost,
			Port: serverPort,
		},
		Mysql: Mysql{
			Host:     mysqlHost,
			Port:     mysqlPort,
			User:     mysqlUser,
			Password: mysqlPassword,
			Database: mysqlDatabase,
		},
		GCP: GCP{
			ProjectID: projectID,
		},
		LogDir: logDir,
		TmpDir: tmpDir,
	}
}

func (c *Config) SetDBConnect() (*sql.DB, error) {
	dataSourceName := strings.Join([]string{
		c.Mysql.User,
		":",
		c.Mysql.Password,
		"@tcp(",
		c.Mysql.Host,
		":",
		c.Mysql.Port,
		")/",
		c.Mysql.Database,
		"?parseTime=true"}, "")

	db, err := sql.Open("mysql", dataSourceName)
	if err != nil {
		return nil, err
	}

	return db, nil
}
