package model

type Request struct {
	Sql     string `json:"sql"`
	Fields  []Field
	Filters []Filter
}

type Field struct {
	Name        string `json:"name"`
	Type        string `json:"type"`
	Description string `json:"description"`
}

type Filter struct {
	Description string `json:"description"`
	Value       string `json:"Value"`
}
