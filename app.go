package main

import (
	"net/http"
	"strings"

	"gitlab.com/satun/go-excel-export/config"
	"gitlab.com/satun/go-excel-export/handler"

	"github.com/labstack/echo/v4"

	_ "github.com/joho/godotenv/autoload"

	_ "github.com/go-sql-driver/mysql"
	_ "google.golang.org/appengine/cloudsql"
)

func main() {
	e := echo.New()
	conf := config.New()

	//if err := checkDir(conf.TmpDir); err != nil {
	//	e.Logger.Fatal(err)
	//}
	//
	//f, err := setLogFile(conf)
	//if err != nil {
	//	e.Logger.Fatal(err)
	//}
	//defer f.Close()

	//e.Logger.SetOutput(f)
	e.HideBanner = true
	//e.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
	//	Output: f,
	//}))

	h := &handler.Handler{
		//DB:     db,
		Config: conf,
	}

	e.POST("/auto-export", h.AutoExport)

	e.GET("/", func(c echo.Context) (err error) {
		return c.String(http.StatusOK, "Status: OK")
	})

	e.Logger.Fatal(e.Start(strings.Join([]string{conf.Server.Host, ":", conf.Server.Port}, "")))
}

//func setLogFile(c *config.Config) (*os.File, error) {
//	if err := checkDir(c.LogDir); err != nil {
//		return nil, err
//	}
//
//	f, err := os.OpenFile(strings.Join([]string{c.LogDir, "main.log"}, ""), os.O_CREATE|os.O_RDWR|os.O_APPEND, 0666)
//	if err != nil {
//		return nil, err
//	}
//
//	return f, nil
//}
//
//func checkDir(d string) error {
//	if _, err := os.Stat(d); err != nil {
//		err := os.MkdirAll(d, os.ModePerm)
//		if err != nil {
//			return err
//		}
//	}
//
//	return nil
//}
